/*
* Main header for file
*
*/

/* Abstract Mode */
/* Fucntion descriptions*/

function abstractModeEnable() {
    var isChecked = abstractModeCheckbox.checked;
    if ( isChecked == true) {
        document.head.appendChild(abstractModeLink);
    }
    if ( isChecked == false ) {
        var removeLink = document.getElementById("abstract");
        document.head.removeChild(removeLink);
    }
}



/* Colorful mode function */
/* Function descriptions */

function colorfulModeEnable() {
    var isChecked = colorfulModeCheckbox.checked;
    if ( isChecked == true) {
        document.head.appendChild(colorfulModeLink);
    }
    if ( isChecked == false ) {
        var removeLink = document.getElementById("colorful");
        document.head.removeChild(removeLink);
    }
}



/* Grid mode function */
/* Function descdriptions */

var gridModeDiv = document.createElement("div");
gridModeDiv.setAttribute("id", "visual_grid");

function gridModeEnable() {
    var isChecked = gridModeCheckbox.checked;
    var horizontalSlots = document.getElementById('horizontalGridSlots').value;
    var verticalSlots = document.getElementById('verticalGridSlots').value;
    var flexWidth = "calc(" + ( 100 / horizontalSlots ) + "% - 2px)";
    if ( isChecked == true ) {
        document.body.insertBefore(gridModeDiv, document.body.firstChild);
        for (let i = 0; i < verticalSlots; i++) {
            for (let i = 0; i < horizontalSlots; i++) {
                var gridModeDivChild = document.createElement("div");
                gridModeDivChild.setAttribute("class", "grid_section");
                gridModeDivChild.style.minWidth = flexWidth;
                gridModeDivChild.style.maxWidth = flexWidth;
                document.getElementById('visual_grid').appendChild(gridModeDivChild);
            }
        }
    }
    if ( isChecked == false ) {
        var thirdsList = gridModeDiv.childNodes;
        var listLength = thirdsList.length;
        for (let i = 0; i < listLength; i++ ) {
            var gridLastChild = gridModeDiv.lastChild;
            gridModeDiv.removeChild(gridLastChild);
        }
        document.body.removeChild(grid);
    }
}



/* 4 - variable detection function */
/* Function descriptions */

function cssVariableDetector(){
    
    var styleSheetNumbers = (document.styleSheets.length) - 1;

    var cssList = getComputedStyle(document.body), j = 0, k = 0;

    for (let i=0; i < cssList.length; i++) {
        var itemSelected = cssList[i];
        var generalVar = /--dbg/g, colorVar = /-cf-/g;
        if (generalVar.test(itemSelected) || colorVar.test(itemSelected)) {
            cssDebugVariableList[k] = itemSelected;
            k++;
        }
        else {
            if ((itemSelected[0] == "-") && (itemSelected[1] == "-")) {
                cssNativeVariableList[j] = itemSelected;
                j++;
            }
        }
    }
    console.log(cssDebugVariableList);
    console.log(cssNativeVariableList);
}

/* Función para cambiar los valores de las variables CSS */

function setCssVariable(event) {
    originalElement = document.getElementById(event.originalTarget.id);
    document.body.style.setProperty(originalElement.id,event.originalTarget.value);
}

/* Función iteradora para listas de variables CSS */

function cssVariableIterator(targetList, targetDiv) {
    for (let i=0; i < targetList.length; i++) {
        var currentVariable = targetList[i];
        var currentVariableStyle = getComputedStyle(document.body).getPropertyValue(currentVariable);
        inputMaker(currentVariable, "text", currentVariableStyle, targetDiv);
    }
}

/* Function description */

function createCssList(){
    var cssList = getComputedStyle(document.body);
    return cssList;
}

/* Function description */

function parseCssList(list){
    var j = 0 , k = 0;
    for (let i=0; i < list.length; i++) {
        var itemSelected = list[i];
        generalVar = /--dbg/g;
        if ((itemSelected[0] == "-") && (itemSelected[1] == "-") ) {
            cssDebugVariableList[j] = itemSelected;
            j++;
        }
        else {
            if ((itemSelected[0] == "-") && (itemSelected[1] == "-")) {
                cssNativeVariableList[k] = itemSelected;
                k++;
            }
        }
    }
}