/*
* Main header for file
*
*/



var jmfStyles = linkGenerator("jmf", "stylesheet", "css/jmf.css");
document.head.appendChild(jmfStyles);



/* Composition of the interface making function */

function menuMaker(){
    //generating main container
    var mainContainer = elementGenerator("div", "nms-main-container");
    mainContainer.setAttribute("draggable", "true");

      //generate menu row 0  and button
      var section0 = sectionGenerator("nms-section0", "nms-section0");
      var section0Title = elementGenerator("p", "nms-title", "nms-title");
      textElementGenerator(section0Title, "Floating div title")

    //generate menu row 1  and button
        var section1 = sectionGenerator("nms-section1", "nms-section1");
        var section1Button = sectionButtonGenerator("1", "section 1", "▼");
        var section1Content = sectionContent("nms-section-1-content");

    //generate checkboxes
        //abstract mode
        var abstractWrapper = elementGenerator ("div", null, "nms-checkbox-wrapper");
        var abstractCBox = elementGenerator ("input", "abstractModeCheckbox");
        inputCheckBoxGenerator(abstractCBox, "Abstract");
        var abstractLabel = elementGenerator("span", "abstract-mode-label", "Abstract mode");
        textElementGenerator(abstractLabel, "Abstract mode");

        //colorful mode
        var colorfulWrapper = elementGenerator ("div", null, "nms-checkbox-wrapper");
        var colorfulCBox = elementGenerator ("input", "colorfulModeCheckbox");
        inputCheckBoxGenerator(colorfulCBox, "Colorful");
        var colorfulLabel = elementGenerator("span", "abstract-mode-label", "Colorful mode");
        textElementGenerator(colorfulLabel, "Colorful mode");

        //grid mode
        var gridWrapper = elementGenerator ("div", null, "nms-checkbox-wrapper");
        var gridCBox = elementGenerator ("input", "gridModeCheckbox");
        inputCheckBoxGenerator(gridCBox, "Grid");
        var gridNumbersWrapper = elementGenerator ("div", "grid-title-wrapper", "");
        

        var gridLabel = elementGenerator("span", "grid-mode-label", "nms-folding-element-button");
        textElementGenerator(gridLabel, "Grid mode");
        var gridCaret = elementGenerator("span", "gridCaret", "nms-caret nms-folding-element-button");
        textElementGenerator(gridCaret, "▼" );
        var gridNumberWrapper2 = elementGenerator("div", "grid-numbers-controls", "number-content-wrapper debug-wrapper nms-folding-element nms-folded-element");
        

        //generate grid numbers
        var gridHorizontal = elementGenerator("input", "horizontalGridSlots");
        inputNumberGenerator(gridHorizontal, 1, 100, 1 );
        var gridHorizontalLabel = labelGenerator(gridHorizontal.id, "Horizontal grid slots");
        var gridVertical = elementGenerator("input", "verticalGridSlots");
        inputNumberGenerator(gridVertical, 1, 100, 1 );
        var gridVerticalLabel = labelGenerator(gridVertical.id, "Vertical grid slots");


    //generate menu row 2  and button

        var section2 = sectionGenerator("section2", "section2");
        var section2Button = sectionButtonGenerator("2", "section 2", "▼");
        var section2Content = sectionContent("nms-section-2-content");


        //generate scrolly div

        
        //generate variable input list
        debugVariableContainer = elementGenerator("div", "debugger-Variable-Container", "nms-variable-container nms-folding-element ");

        //generate debug vars button
        debugVariablesButton = foldingButton("debug-vars-button", "Debug Variables", null);

        //generate debug vars div
        var debugVariableScrolly = elementGenerator( "div", "debug-vars", "nms-variable-list nms-folding-element nms-folded-element");

        //generate variable input list
        websiteVariableContainer = elementGenerator("div", "website-Variable-Container", "nms-variable-container nms-folding-element ");

        //generate debug vars button
        websiteVariablesButton = foldingButton("website-vars-button", "Website Variables", null);

        //generate native vars scrolly div
        var websiteVariableScrolly = elementGenerator( "div", "website-vars", "nms-variable-list nms-folding-element nms-folded-element");


        debugVariableContainer.appendChild(debugVariablesButton);
        debugVariableContainer.appendChild(debugVariableScrolly);

        websiteVariableContainer.appendChild(websiteVariablesButton);
        websiteVariableContainer.appendChild(websiteVariableScrolly);

    
    //appending all the elements
    document.body.appendChild(mainContainer);
        //append title
        mainContainer.appendChild(section0);
        section0.appendChild(section0Title);

        mainContainer.appendChild(section1);
            //append folding button


            section1.appendChild(section1Button);
            //append hidden content
            section1.appendChild(section1Content);
            //append inner content
                section1Content.appendChild(abstractWrapper);
                    abstractWrapper.appendChild(abstractCBox);
                    abstractWrapper.appendChild(abstractLabel);

                section1Content.appendChild(colorfulWrapper);
                    colorfulWrapper.appendChild(colorfulCBox);
                    colorfulWrapper.appendChild(colorfulLabel);
                
                section1Content.appendChild(gridWrapper);
                    gridWrapper.appendChild(gridCBox);
                    gridWrapper.appendChild(gridNumbersWrapper);
                        gridNumbersWrapper.appendChild(gridLabel);
                        gridNumbersWrapper.appendChild(gridCaret);
                    gridWrapper.appendChild(gridNumberWrapper2);
                            gridNumberWrapper2.appendChild(gridHorizontal);
                            gridNumberWrapper2.appendChild(gridHorizontalLabel);
                            gridNumberWrapper2.appendChild(gridVertical);
                            gridNumberWrapper2.appendChild(gridVerticalLabel);

                

        mainContainer.appendChild(section2);
            //append folding button
            section2.appendChild(section2Button);
            //append hidden content
            section2.appendChild(section2Content);
                section2Content.appendChild(debugVariableContainer);
                section2Content.appendChild(websiteVariableContainer);




    foldingButtonGenerator("#nms-section-1-button", "#nms-section-1-content", "nms-caret", "animation-rotate-90");
    foldingButtonGenerator("#grid-title-wrapper", "#grid-numbers-controls", "nms-caret", "animation-rotate-90");
    foldingButtonGenerator("#nms-section-2-button", "#nms-section-2-content", "nms-caret", "animation-rotate-90");
    foldingButtonGenerator("#debug-vars-button", "#debug-vars", "nms-caret", "animation-rotate-90");
    foldingButtonGenerator("#website-vars-button", "#website-vars", "nms-caret", "animation-rotate-90");


    document.getElementById('gridModeCheckbox').addEventListener("change", gridModeEnable);
    document.getElementById('abstractModeCheckbox').addEventListener("change", abstractModeEnable);
    document.getElementById('colorfulModeCheckbox').addEventListener("change", colorfulModeEnable);


    cssVariableIterator(cssDebugVariableList, "debug-vars");

    cssVariableIterator(cssNativeVariableList, "website-vars");

    if(browser != "Chrome"){
        cssVariableIterator(cssDebugVariableList, "debug-vars");
    } else {
        let debugVariableBrowseralert = browserWarningGenerator(browser);
        debugVariableScrolly.appendChild(debugVariableBrowseralert);
    }

    if(browser != "Chrome"){
        cssVariableIterator(cssNativeVariableList, "debug-vars");
    } else {
        let websiteVariableBrowseralert = browserWarningGenerator(browser);
        websiteVariableScrolly.appendChild(websiteVariableBrowseralert);
    }


    var dm = document.getElementById('nms-main-container');
    
    mainContainer.addEventListener('dragstart',drag_start,false);
    document.body.addEventListener('dragover',drag_over,false);
    document.body.addEventListener('drop',drop,false);
}

/* Creation of variable lists */

cssDebugVariableList = [], cssNativeVariableList = [];

setTimeout(cssVariableDetector, 50);

console.log(browser);
setTimeout(menuMaker, 100);